package com.wirelessscanner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ActivityGroup;
import android.app.ActivityManager;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TabHost;

import com.wirelessscanner.controller.TelController;
import com.wirelessscanner.dao.DAOTelephony;
import com.wirelessscanner.models.DTOTelephony;
import com.wirelessscanner.tabActivities.NFCActivity;
import com.wirelessscanner.tabActivities.TelActivity;
import com.wirelessscanner.tabActivities.WiFiActivity;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class MainActivity extends ActivityGroup {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.manageTabs();
        this.RequestPermissions();
    }


    private void manageTabs(){
        TabHost th = (TabHost)findViewById(R.id.tabHostMain);


        th.setup(this.getLocalActivityManager());
        TabHost.TabSpec ts1 = th.newTabSpec("Tel");

        ts1.setContent(new Intent(this, TelActivity.class));
        ts1.setIndicator("Tel");
        th.addTab(ts1);

        TabHost.TabSpec ts2 = th.newTabSpec("WiFi");
        th.setup(this.getLocalActivityManager());
        ts2.setContent(new Intent(this, WiFiActivity.class));
        ts2.setIndicator("WiFi");
        th.addTab(ts2);

        TabHost.TabSpec ts3 = th.newTabSpec("NFC");
        th.setup(this.getLocalActivityManager());
        ts3.setContent(new Intent(this, NFCActivity.class));
        ts3.setIndicator("NFC");
        th.addTab(ts3);
    }

    protected void RequestPermissions(){
        String[] permissions = {
                "android.permission.READ_PHONE_STATE",
                "android.permission.READ_PRIVILEGED_PHONE_STATE",
                "android.permission.ACCESS_FINE_LOCATION",
                "android.permission.ACCESS_WIFI_STATE",
                "android.permission.NFC"
        };
        int requestCode = 5;
        ActivityCompat.requestPermissions(this,permissions, requestCode);
    }
}