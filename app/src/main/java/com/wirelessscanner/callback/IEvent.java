package com.wirelessscanner.callback;

public interface IEvent {
    void onChange(Object e);
}
