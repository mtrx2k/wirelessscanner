package com.wirelessscanner.callback;

import android.util.Log;

import java.util.ArrayList;

public class OnChangeEventHandler {

    private ArrayList<IEvent> events;

    public OnChangeEventHandler(){
        this.events = new ArrayList<IEvent>();
    }

    public void add(IEvent e){this.events.add(e);}
    public void emit(Object e){
        for (int i = 0; i < this.events.size(); i++)
            this.events.get(i).onChange(e);
    }
}
