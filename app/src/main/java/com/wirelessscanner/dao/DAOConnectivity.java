package com.wirelessscanner.dao;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;

import com.wirelessscanner.helper.ConnectivityHelper;
import com.wirelessscanner.models.DTOConnBandwith;
import com.wirelessscanner.models.DTOConnectivity;

public class DAOConnectivity {

    private Context c;
    private ConnectivityManager cManager;


    public DAOConnectivity(Context context){
        this.c = context;
        this.cManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public DTOConnectivity load(){
        DTOConnectivity dto = new DTOConnectivity();
        NetworkInfo ni = this.cManager.getActiveNetworkInfo();

        dto.Type = ConnectivityHelper.convertType(ni.getType());
        dto.IsRoaming = ni.isRoaming();
        dto.State = ni.getState().toString();
        dto.Reason = ni.getReason();
        dto.FurtherInfo = ni.getExtraInfo();
        dto.TypeName = ni.getTypeName();
        dto.SubTypeName = ni.getSubtypeName();

        return dto;
    }

    public DTOConnBandwith measureBandwith(){
        DTOConnBandwith dto = new DTOConnBandwith();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            NetworkCapabilities nc = cManager.getNetworkCapabilities(cManager.getActiveNetwork());
            dto.Downstream = nc.getLinkDownstreamBandwidthKbps();
            dto.Upstream = nc.getLinkDownstreamBandwidthKbps();
        }
        else
            return null;
        return dto;
    }
}
