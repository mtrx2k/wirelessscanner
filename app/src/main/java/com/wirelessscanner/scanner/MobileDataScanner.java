package com.wirelessscanner.scanner;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.UiccCardInfo;
import com.wirelessscanner.models.DTOSIMData;

import java.util.List;

public class MobileDataScanner {

    private TelephonyManager telManager;

    public MobileDataScanner(Context appContext){
        telManager = (TelephonyManager)appContext.getSystemService(Context.TELEPHONY_SERVICE);
    }

    public DTOSIMData scan(){
        DTOSIMData data = new DTOSIMData();


        data.countryiso = telManager.getNetworkCountryIso();
        data.simOperator = telManager.getSimOperator();
        data.simOperatorName = telManager.getSimOperatorName();
        //data.simSerialNumber = telManager.getSimSerialNumber();
        data.simStateID = telManager.getSimState();
        //data.subscriberID = telManager.getSubscriberId();

        return data;
    }
}
