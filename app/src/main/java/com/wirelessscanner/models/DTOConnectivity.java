package com.wirelessscanner.models;

import android.util.Pair;

import com.wirelessscanner.helper.StuffJavaCantDoHelper;

import java.util.ArrayList;

public class DTOConnectivity {


    public String Type;
    public boolean IsRoaming;
    public String State;
    public String Reason;
    public String FurtherInfo;
    public String SubTypeName;
    public String TypeName;

    public ArrayList<Pair<String, String>> parse(){
        ArrayList<Pair<String, String>> data = new ArrayList<Pair<String, String>>();

        if (!StuffJavaCantDoHelper.isNullOrEmpty(Type)) data.add(new Pair<String, String>("Type", this.Type));
        data.add(new Pair<String, String>("Roaming", String.valueOf(IsRoaming)));
        if (!StuffJavaCantDoHelper.isNullOrEmpty(State)) data.add(new Pair<String, String>("State", this.State));
        if (!StuffJavaCantDoHelper.isNullOrEmpty(Reason)) data.add(new Pair<String, String>("Reason", this.Reason));
        if (!StuffJavaCantDoHelper.isNullOrEmpty(FurtherInfo)) data.add(new Pair<String, String>("Further Info", this.FurtherInfo));
        if (!StuffJavaCantDoHelper.isNullOrEmpty(TypeName)) data.add(new Pair<String, String>("Type Name", this.TypeName));
        if (!StuffJavaCantDoHelper.isNullOrEmpty(SubTypeName)) data.add(new Pair<String, String>("Subtype Name", this.SubTypeName));

        return data;
    }
}
