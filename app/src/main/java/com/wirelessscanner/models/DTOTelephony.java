package com.wirelessscanner.models;

import android.telephony.CellIdentityCdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellLocation;
import android.telephony.cdma.CdmaCellLocation;
import android.util.Log;
import android.util.Pair;

import com.wirelessscanner.callback.OnChangeEventHandler;
import com.wirelessscanner.helper.TelHelper;

import java.util.ArrayList;
import java.util.List;

public class DTOTelephony {

    public DTOTelephony(){this.onCellsLoaded = new OnChangeEventHandler();}

    public OnChangeEventHandler onCellsLoaded;
    public CdmaCellLocation CellLoc;
    public String IMEINumber;
    public String subscriberID;
    public String SIMSerialNumber;
    public String networkCountryISO;
    public String SIMCountryISO;
    public String softwareVersion;
    public String voiceMailNumber;
    public int VoiceNetworkType;
    public int DataNetworkType;
    public List<CellInfo> CellInfo;
    public String SimOperator;

    public ArrayList<Pair<String, String>> GetInfos(){
        ArrayList<Pair<String, String>> data = new ArrayList<Pair<String, String>>();

        if (this.IMEINumber != null) data.add(new Pair<String, String>("IMEI", this.IMEINumber.toString()));
        if (this.subscriberID != null) data.add(new Pair<String, String>("Subscriber ID", this.subscriberID.toString()));
        if (this.SIMSerialNumber != null) data.add(new Pair<String, String>("SIM Serial number", this.SIMSerialNumber.toString()));
        if (this.networkCountryISO != null) data.add(new Pair<String, String>("Network Country ISO", this.networkCountryISO.toString()));
        if (this.SIMCountryISO != null) data.add(new Pair<String, String>("SIM Country ISO", this.SIMCountryISO.toString()));
        if (this.softwareVersion != null) data.add(new Pair<String, String>("Software Version", this.softwareVersion.toString()));
        data.add(new Pair<String, String>("VoiceNetwork Type", TelHelper.getNetType(VoiceNetworkType)));
        data.add(new Pair<String, String>("DataNetwork Type", TelHelper.getNetType(DataNetworkType)));
        if (this.SimOperator != null) data.add(new Pair<String, String>("SIM Operator", this.SimOperator.toString()));


        return data;
    }

    public ArrayList<Pair<String, String>> getCellInfos(){
        ArrayList<Pair<String, String>> data = new ArrayList<Pair<String, String>>();
        if(this.CellInfo == null)
            return data;

        for (int i = 0; i < this.CellInfo.size(); i++){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                CellIdentityCdma cdma;
                try{
                    cdma = ((CellInfoCdma) this.CellInfo.get(i)).getCellIdentity();
                }
                catch (Exception e){continue;}
                data.add(
                        new Pair<String, String>(String.valueOf(cdma.getBasestationId()),
                                String.valueOf(cdma.getNetworkId())));
            }
        }

        return data;
    }
}
