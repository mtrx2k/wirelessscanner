package com.wirelessscanner.models;

public class DTOSIMData {
    public String countryiso;
    public String simOperator;
    public String simOperatorName;
    public String simSerialNumber;
    public String simState;
    public int simStateID;
    public String subscriberID;
}
