package com.wirelessscanner.models;

import android.content.Context;
import android.graphics.Paint;
import android.net.DhcpInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;
import android.util.Pair;

import com.wirelessscanner.helper.Ipv4Helper;
import com.wirelessscanner.helper.WiFiHelper;

import java.util.ArrayList;
import java.util.List;

public class DTOWiFi {

    public int levels = 5;

    public List<ScanResult> ScanResults;
    public boolean IsWifiEnabled;
    public WifiInfo ConnectionInfo;
    public DhcpInfo DhcpInfo;
    public int WiFiState;
    public boolean Is5GhzSupported;

    public ArrayList<Pair<String, String>> getCurrentInfo(){
        ArrayList<Pair<String, String>> data = new ArrayList<Pair<String, String>>();
        if (ConnectionInfo == null)
            return data;

        data.add(new Pair<String, String>("SSID", ConnectionInfo.getSSID()));
        data.add(new Pair<String, String>("BSSID", ConnectionInfo.getBSSID()));
        data.add(new Pair<String, String>("MAC", ConnectionInfo.getMacAddress()));
        data.add(new Pair<String, String>("LinkSpeed (Mbps)", String.valueOf(ConnectionInfo.getLinkSpeed())));
        data.add(new Pair<String, String>("RSSI", WiFiHelper.connectionStateConvert(ConnectionInfo.getRssi())));
        data.add(new Pair<String, String>("Network ID", String.valueOf(ConnectionInfo.getNetworkId())));
        data.add(new Pair<String, String>("IP", Ipv4Helper.GetV4Adress(ConnectionInfo.getIpAddress())));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            data.add(new Pair<String, String>("Frequency (MHz)", String.valueOf(ConnectionInfo.getFrequency())));
            data.add(new Pair<String, String>("Frequency (GHz)", String.valueOf(ConnectionInfo.getFrequency()/1000)));
        }

        return data;
    }

    public ArrayList<Pair<String, String>> getDhcpInfo(){
        ArrayList<Pair<String, String>> data = new ArrayList<Pair<String, String>>();
        if (DhcpInfo == null)
            return data;

        data.add(new Pair<String, String>("DNS1", Ipv4Helper.GetV4Adress(DhcpInfo.dns1)));
        data.add(new Pair<String, String>("DNS2", Ipv4Helper.GetV4Adress(DhcpInfo.dns2)));
        data.add(new Pair<String, String>("Gateway", Ipv4Helper.GetV4Adress(DhcpInfo.gateway)));
        data.add(new Pair<String, String>("IP", Ipv4Helper.GetV4Adress(DhcpInfo.ipAddress)));
        data.add(new Pair<String, String>("Netmask", Ipv4Helper.GetV4Adress(DhcpInfo.netmask)));
        data.add(new Pair<String, String>("Server", Ipv4Helper.GetV4Adress(DhcpInfo.serverAddress)));
        data.add(new Pair<String, String>("Lease", String.valueOf(DhcpInfo.leaseDuration)));

        return data;
    }

    public ArrayList<Pair<String, String>> getWiFiInfo(){
        ArrayList<Pair<String, String>> data = new ArrayList<Pair<String, String>>();

        data.add(new Pair<String, String>("State", WiFiHelper.stateConvert(WiFiState)));
        data.add(new Pair<String, String>("IsWifiEnabled", String.valueOf(IsWifiEnabled)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) data.add(new Pair<String, String>("5Ghz Supported", String.valueOf(Is5GhzSupported)));
        return data;
    }


}
