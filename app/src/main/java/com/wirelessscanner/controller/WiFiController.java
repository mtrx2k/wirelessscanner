package com.wirelessscanner.controller;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Pair;
import android.widget.GridView;

import com.wirelessscanner.R;
import com.wirelessscanner.dao.DAOWiFi;
import com.wirelessscanner.helper.EConnectionState;
import com.wirelessscanner.helper.WiFiHelper;
import com.wirelessscanner.models.DTOWiFi;

import java.util.ArrayList;

public class WiFiController extends Thread{

    private DAOWiFi dao;
    private Context c;
    private GridViewController gvc_scan;
    private GridViewController gvc_data;
    private GridViewController gvc_current_data;
    private GridViewController gvc_dhcp_data;

    public WiFiController(Activity ac){
        this.c = ac;
        this.gvc_scan = new GridViewController((GridView) ac.findViewById(R.id.wifi_scan_grid), c);
        this.gvc_data = new GridViewController((GridView) ac.findViewById(R.id.wifi_data_grid), c);
        this.gvc_current_data = new GridViewController((GridView) ac.findViewById(R.id.wifi_current_data_grid), c);
        this.gvc_dhcp_data = new GridViewController((GridView) ac.findViewById(R.id.wifi_dhcp_grid), c);
        this.dao = new DAOWiFi(ac);
    }

    public void load(){
        start();
    }

    @Override
    public void run(){
        while (true){
            final DTOWiFi data = this.dao.getInfos();

            ((Activity)this.c).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadWifiScans(data);
                    loadCurrentWiFi(data);
                    loadWifiData(data);
                    loadDHCPData(data);
                }
            });
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadWifiData(DTOWiFi dto){
        ArrayList<Pair<String, String>> data = dto.getWiFiInfo();
        gvc_data.clearInfo();
        for (int i = 0; i < data.size(); i++)
            gvc_data.addInfo(data.get(i).first, String.valueOf(data.get(i).second));
    }

    private void loadDHCPData(DTOWiFi dto){
        if (dto.DhcpInfo == null)
            return;

        ArrayList<Pair<String, String>> data = dto.getDhcpInfo();

        gvc_dhcp_data.clearInfo();
        for (int i = 0; i < data.size(); i++)
            gvc_dhcp_data.addInfo(data.get(i).first, String.valueOf(data.get(i).second));
    }

    private void loadCurrentWiFi(DTOWiFi dto){
        ArrayList<Pair<String, String>> data = dto.getCurrentInfo();
        gvc_current_data.clearInfo();
        for (int i = 0; i < data.size(); i++){
            gvc_current_data.addInfo(data.get(i).first, data.get(i).second);
        }
    }

    private void loadWifiScans(DTOWiFi dto){
        if (dto.ScanResults == null)
            return;

        gvc_scan.clearInfo();
        gvc_scan.addInfo("Name", "Connectivity");
        for (int i = 0; i < dto.ScanResults.size(); i++)
            gvc_scan.addInfo(dto.ScanResults.get(i).SSID, WiFiHelper.connectionStateConvert(dto.ScanResults.get(i).level));
    }
}
