package com.wirelessscanner.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.util.Log;
import android.util.Pair;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import com.wirelessscanner.MainActivity;
import com.wirelessscanner.R;
import com.wirelessscanner.callback.IEvent;
import com.wirelessscanner.dao.DAOConnectivity;
import com.wirelessscanner.dao.DAOTelephony;
import com.wirelessscanner.models.DTOConnBandwith;
import com.wirelessscanner.models.DTOConnectivity;
import com.wirelessscanner.models.DTOTelephony;

import java.util.ArrayList;

public class TelController extends Thread implements IEvent {
    private Context c;
    private Activity ac;
    private DAOTelephony dao;
    private DAOConnectivity dao_conn;
    private GridViewController cgv;
    private GridViewController cgv_cells;
    private GridViewController cgv_bandwith;
    private GridViewController cgv_conn;

    public TelController(Activity ac){
        this.c = ac;
        this.ac = ac;
        this.cgv = new GridViewController((GridView) this.ac.findViewById(R.id.tel_grid_view), c);
        this.cgv_cells = new GridViewController((GridView) this.ac.findViewById(R.id.tel_grid_view_cells), c);
        this.cgv_bandwith = new GridViewController((GridView) this.ac.findViewById(R.id.tel_grid_bandwith), c);
        this.cgv_conn = new GridViewController((GridView) this.ac.findViewById(R.id.tel_grid_conn), c);
        this.dao = new DAOTelephony(c);
        this.dao_conn = new DAOConnectivity(c);
    }

    public void load(){
        start();
    }

    private void loadSIMInfo(DTOTelephony dto){
        ArrayList<Pair<String, String>> data = dto.GetInfos();
        this.cgv.clearInfo();
        for (int i = 0; i < data.size(); i++){
            this.cgv.addInfo(data.get(i).first, data.get(i).second);
        }
    }

    private void loadCellInfo(DTOTelephony dto){
        ArrayList<Pair<String, String>> data = dto.getCellInfos();
        cgv_cells.clearInfo();

        cgv_cells.addInfo("BaseStationID", "NetID");

        for (int i = 0; i < data.size(); i++){
            this.cgv_cells.addInfo(data.get(i).first, data.get(i).second);
        }
    }

    private void loadBandwith(){
        DTOConnBandwith dto = this.dao_conn.measureBandwith();
        cgv_bandwith.clearInfo();
        cgv_bandwith.addInfo("Downstream", String.valueOf(dto.Downstream));
        cgv_bandwith.addInfo("Upstream", String.valueOf(dto.Upstream));
    }

    public void showCellOnMap(){
        DTOTelephony telDTO = this.dao.GetInfos();
        if (telDTO.CellLoc == null)
            return;
        CdmaCellLocation loc = telDTO.CellLoc;
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + loc.getBaseStationLatitude() + "," + loc.getBaseStationLongitude());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        this.ac.startActivity(mapIntent);

    }

    private void loadConnectivity(){
        DTOConnectivity dto = dao_conn.load();
        cgv_conn.clearInfo();
        ArrayList<Pair<String, String>> parsed = dto.parse();
        for (int i = 0; i < parsed.size(); i++)
            cgv_conn.addInfo(parsed.get(i).first, parsed.get(i).second);
    }


    @Override
    public void onChange(Object e) {
        this.loadCellInfo((DTOTelephony) e);
    }

    @Override
    public void run() {
        while(true){
            final DTOTelephony dto = this.dao.GetInfos();
            dto.onCellsLoaded.add(this);

            this.ac.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadSIMInfo(dto);
                    loadCellInfo(dto);
                    loadConnectivity();
                    loadBandwith();
                }
            });
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
