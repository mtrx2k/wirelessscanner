package com.wirelessscanner.helper;

//Class made espacially for Java <3
//C# is able to do this.
public class StuffJavaCantDoHelper {

    public static boolean isNullOrEmpty(String s){
        //Strings can be empty
        //But who guessed that strings can also be null :O
        return (s == null || s.isEmpty());
    }
}
