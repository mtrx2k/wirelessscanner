package com.wirelessscanner.helper;

public enum  EConnectionState {
    VeryGood, Good, Medium, Bad, Worse
}
