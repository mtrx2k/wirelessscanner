package com.wirelessscanner.helper;

import android.util.Log;

import java.math.BigInteger;
import java.net.InetAddress;

public class Ipv4Helper {

    public static String GetV4Adress(int addr){
        byte[] bytes_tmp = BigInteger.valueOf(addr).toByteArray();
        byte[] bytes = new byte[bytes_tmp.length];
        for (int i = 0; i < bytes_tmp.length; i++)
            bytes[bytes_tmp.length-1-i] = bytes_tmp[i];
        InetAddress address;
        try{
             address = InetAddress.getByAddress(bytes);
        }
        catch (Exception e){return "0.0.0.0";}
        if (address == null)
            return "0.0.0.0";
        return address.getHostAddress();
    }
}
