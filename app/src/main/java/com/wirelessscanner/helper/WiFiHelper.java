package com.wirelessscanner.helper;

import android.net.wifi.WifiManager;

public class WiFiHelper {

    public static String stateConvert(int state){
        switch (state){
            case WifiManager.WIFI_STATE_DISABLED:
                return "Disabled";
            case WifiManager.WIFI_STATE_DISABLING:
                return "Disabling";
            case WifiManager.WIFI_STATE_ENABLED:
                return "Enabled";
            case WifiManager.WIFI_STATE_ENABLING:
                return "Enabling";
            case WifiManager.WIFI_STATE_UNKNOWN:
                return "unknown";
            default:
                return "unknown";
        }
    }


    public static String connectionStateConvert(int i){
        return String.valueOf(EConnectionState.values()[4-WifiManager.calculateSignalLevel(i, 5)]);
    }
}
